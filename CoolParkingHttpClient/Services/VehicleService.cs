using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using CoolParking.BL.Models;
namespace CoolParkingHttpClient.Services
{
    public class VehicleService
    {
        [Required] 
        [JsonPropertyName("id")] 
        public string Id { get; set; }
        
        [Required]
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [Required]
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }

        public VehicleService() {}

        public override string ToString()
        {
            return $"Id: {Id}\nVehicleType: {VehicleType}\nBalance: {Balance}\n";
        }
    }
}